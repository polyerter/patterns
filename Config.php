<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 24.01.2018
 * Time: 13:04
 */

namespace Config;

class Config
{
	public $offline         = '0';
	public $offline_message = 'Сайт закрыт на техническое обслуживание.<br />Пожалуйста, зайдите позже.';
	public $debug           = '0';
	public $dbtype          = 'mysqli';
	public $host            = 'localhost';
	public $user            = 'demo';
	public $password        = 'asdasd';
	public $db              = 'polyerter';
	public $dbprefix        = 'r41c0_';
	public $mailfrom        = 'polyerter@tspu.edu.ru';
}