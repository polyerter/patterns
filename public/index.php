<?php
chdir(dirname(__DIR__));
require 'vendor/autoload.php';

//use Framework\Http\Request;
//use Framework\Patterns\Registry;
//use Framework\Other\ParamHandle;
use Framework\Adapter;
use Framework\Decorator;
use Framework\State;
use Framework\FactoryMethod;
use Framework\AbstractFactory;
use Framework\Strategy\StrategyTutorial;
use Framework\Builder;
use Framework\Iterator;

//$request = new Request();
//$request->a = ['b'=>'c'];

/* USING OF REGISTRY */
//$params = ParamHandle::getInstance('./file.xml');
//$params->addParam('name', 'First product');
//$params->addParam('name1', 'Second product');
//$r = $params->getAllParams();
//print_r($r);

/*  Adapter  */
Adapter\Client::main();

/*  Decorator  */
Decorator\Shop::main();

/*  State  */
State\StateTutorial::main();

/*  State  */
FactoryMethod\FactoryTutorial::main();

/*  AbstractFactory  */
AbstractFactory\Client::main();

/*  Strategy  */
StrategyTutorial::main();

/*  Builder  */
Builder\Client::main();

/*  Iterator  */
Iterator\IteratorTutorial::main();