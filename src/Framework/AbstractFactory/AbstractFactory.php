<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 15:24
 */

namespace Framework\AbstractFactory;

interface AbstractFactory
{
	function getCake(): Cake;

	function getMilkCocktail(): MilkCocktail;
}