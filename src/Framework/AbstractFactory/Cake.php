<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 15:40
 */

namespace Framework\AbstractFactory;


interface Cake
{
	function printName();

	function printPrice();
}