<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 15:39
 */

namespace Framework\AbstractFactory;


class ChocolateCake implements Cake
{
	function printName()
	{
		echo self::class . "\n";
	}

	function printPrice()
	{
		echo 50;
	}

}