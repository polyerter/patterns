<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 15:49
 */

namespace Framework\AbstractFactory;


class ChocolateFactory implements AbstractFactory
{
	public function getCake(): Cake
	{
		return new ChocolateCake();
	}

	public function getMilkCocktail(): MilkCocktail
	{
		return new ChocolateMilkCocktail();
	}

}