<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 15:40
 */

namespace Framework\AbstractFactory;


class ChocolateMilkCocktail implements MilkCocktail
{
	function printName()
	{
		echo self::class . "\n";
	}

	function printPrice()
	{
		echo 35;
	}
}