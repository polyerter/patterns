<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 15:51
 */

namespace Framework\AbstractFactory;


class Client
{
	public static function main()
	{
		$factory = new ChocolateFactory();

		$factory->getCake()->printName();
		$factory->getMilkCocktail()->printName();
	}

}