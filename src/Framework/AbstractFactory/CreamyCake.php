<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 15:39
 */

namespace Framework\AbstractFactory;


class CreamyCake implements Cake
{
	function printName()
	{
		echo self::class . "\n";
	}

	function printPrice()
	{
		echo 55;
	}

}