<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 15:50
 */

namespace Framework\AbstractFactory;


class CreamyFactory implements AbstractFactory
{
	public function getCake(): Cake
	{
		return new CreamyCake();
	}

	public function getMilkCocktail(): MilkCocktail
	{
		return new CreamyMilkCocktail();
	}

}