<?php

namespace Framework\Adapter;


class Client
{
	public static function main()
	{
		(new PrinterSecond())->consolePrint();

		$printInterface = new PrinterComp(new Printer());
		$printInterface->consolePrint();
	}
}