<?php

namespace Framework\Adapter;


interface PrintInterface
{
	public function consolePrint();
}