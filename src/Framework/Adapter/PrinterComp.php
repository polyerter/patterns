<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 14.11.2018
 * Time: 14:42
 */

namespace Framework\Adapter;


class PrinterComp implements PrintInterface
{
	private $printer;

	public function __construct(Printer $printer)
	{
		$this->printer = $printer;
	}

	public function consolePrint()
	{
		$this->printer->print();
	}

}