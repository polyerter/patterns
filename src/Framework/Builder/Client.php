<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 14:31
 */

namespace Framework\Builder;


class Client
{

	public static function main()
	{
		$headman = new Headman();

		$headman->setBuilder(new StrongComputerBuilder());
		$computer = $headman->buildComputer();

		echo $computer;
	}

}