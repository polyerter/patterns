<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 14:14
 */

namespace Framework\Builder;


class Computer
{
	private $ram;
	private $hdd;
	private $processor;

	public function setRam(int $ram)
	{
		$this->ram = $ram;
	}

	public function setHdd(HDD $hdd)
	{
		$this->hdd = $hdd->getRam();
	}

	public function setProcessor(string $processor)
	{
		$this->processor = $processor;
	}

	public function __toString()
	{
		$computer = [
			'Computer'=>[
				'ram'=>$this->ram,
				'hdd'=>$this->hdd,
				'processor'=>$this->processor,
			]
		];

		return json_encode($computer);
	}

}