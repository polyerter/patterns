<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 14:23
 */

namespace Framework\Builder;


abstract class ComputerBuilder
{
	public $computer;

	public function createComputer()
	{
		$this->computer = new Computer();
	}

	public function getComputer()
	{
		return $this->computer;
	}

	abstract function buildHDD();

	abstract function buildProcessor();

	abstract function buildRam();
}