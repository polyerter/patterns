<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 14:14
 */

namespace Framework\Builder;


interface HDD
{
	function getRam();
}