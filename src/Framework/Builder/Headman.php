<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 14:28
 */

namespace Framework\Builder;


class Headman
{
	private $builder;

	public function setBuilder(ComputerBuilder $builder)
	{
		$this->builder = $builder;
	}

	public function buildComputer() : Computer
	{
		$this->builder->createComputer();
		$this->builder->buildProcessor();
		$this->builder->buildHdd();
		$this->builder->buildRam();

		return $this->builder->getComputer();
	}

}