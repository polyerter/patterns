<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 14:21
 */

namespace Framework\Builder;


class SATATwo implements HDD
{
	public function getRam()
	{
		return "SATA_2";
	}
}