<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 14:25
 */

namespace Framework\Builder;


class StrongComputerBuilder extends ComputerBuilder
{
	function buildHDD()
	{
		$this->computer->setHdd(new SATATree());
	}

	function buildProcessor()
	{
		$this->computer->setProcessor("i7 3.1");
	}

	function buildRam()
	{
		$this->computer->setRam(16000);
	}
}