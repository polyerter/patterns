<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 14:27
 */

namespace Framework\Builder;


class WeakComputerBuilder extends ComputerBuilder
{
	function buildHdd()
	{
		$this->computer->setHdd(new SATATwo());
	}

	function buildProcessor()
	{
		$this->computer->setProcessor("i3 1.8");
	}

	function buildRam()
	{
		$this->computer->setRam(2000);
	}
}