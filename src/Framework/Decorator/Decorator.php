<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 14.11.2018
 * Time: 14:58
 */

namespace Framework\Decorator;


abstract class Decorator implements Product
{
	public $product;

	protected function __construct(Product $product)
	{
		$this->product = $product;
	}
}