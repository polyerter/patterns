<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 14.11.2018
 * Time: 14:57
 */

namespace Framework\Decorator;


class Milk implements Product
{
	public $price;

	public function __construct($price)
	{
		$this->price = $price;
	}

	public function getPrice()
	{
		return $this->price;
	}
}