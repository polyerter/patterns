<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 14.11.2018
 * Time: 15:28
 */

namespace Framework\Decorator;


class MilkDiscount extends Decorator
{
	public function __construct(Product $product)
	{
		parent::__construct($product);
	}

	public function getPrice()
	{
		return $this->product->getPrice() - 15;
	}

}