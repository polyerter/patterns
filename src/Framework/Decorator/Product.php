<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 14.11.2018
 * Time: 14:56
 */

namespace Framework\Decorator;


interface Product
{
	public function getPrice();
}