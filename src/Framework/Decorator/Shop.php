<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 14.11.2018
 * Time: 15:27
 */

namespace Framework\Decorator;


class Shop
{
	public static function main()
	{
		$milk = new Milk(50);
		$milkDiscount = new MilkDiscount($milk);
		$p = $milkDiscount->getPrice();

		print_r($p);
	}

}