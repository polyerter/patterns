<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 14:13
 */

namespace Framework\FactoryMethod;


class BlackChocolate implements Chocolate
{
	private $price = 15;

	function printPrice()
	{
		echo $this->price;
	}
}