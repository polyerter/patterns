<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 14:12
 */

namespace Framework\FactoryMethod;


interface Chocolate
{
	function printPrice();
}