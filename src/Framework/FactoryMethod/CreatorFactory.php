<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 14:17
 */

namespace Framework\FactoryMethod;

use Exception;

class CreatorFactory extends Factory
{
	function create(): Chocolate
	{
		if ($this->flag == 0)
			return new WhiteChocolate();
		else if ($this->flag == 1)
			return new BlackChocolate();

		throw new Exception('Not Flag');
	}

}