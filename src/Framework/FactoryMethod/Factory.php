<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 14:15
 */

namespace Framework\FactoryMethod;


abstract class Factory
{
	protected $flag;

	public function __construct(int $flag)
	{
		$this->flag = $flag;
	}

	abstract function create(): Chocolate;

}