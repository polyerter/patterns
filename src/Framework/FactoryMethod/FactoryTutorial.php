<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 16.11.2018
 * Time: 14:12
 */

namespace Framework\FactoryMethod;


class FactoryTutorial
{
	public static function main()
	{
		$factory = new CreatorFactory(1);
		$chocolate = $factory->create();

		$chocolate->printPrice();

	}


}