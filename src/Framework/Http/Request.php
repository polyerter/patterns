<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 25.12.2017
 * Time: 14:00
 */

namespace Framework\Http;

class Request
{

	public function getQueryParams(): array
	{
		return $_GET;
	}

	public function getParsedBody(): array
	{
		return $_POST ?: array();
	}

	public function getMethod()
	{

	}

}