<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.12.2018
 * Time: 12:55
 */

namespace Framework\Iterator;

class IteratorTutorial
{
	public static function main()
	{
		$arr = [1, 2, 3];

		$iterator = new SimpleIterator($arr);

		while ($iterator->hasNext())
		{
			print_r("\n" .$iterator->next()."\n");
		}
	}


}