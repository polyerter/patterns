<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.12.2018
 * Time: 12:54
 */

namespace Framework\Iterator;

class SimpleIterator
{
	private $Object = [];
	private $index = 0;

	public function __construct($object =array())
	{
		$this->Object = $object;
	}

	public function next()
	{
		return $this->Object[$this->index++];
	}

	public function hasNext(): bool
	{
		return $this->index < count($this->Object);
	}

}