<?php

namespace Framework\Other;

//use Framework\Patterns\Registry;

abstract class ParamHandle
{
	protected $source;
	protected $params = array();

	function __construct($source)
	{
		$this->source = $source;
	}

	function addParam($key, $val)
	{
		$this->params[$key] = $val;
	}

	function getAllParams()
	{
		return $this->params;
	}

	static function getInstance($filename = null)
	{
		if (is_null($filename)) return null;

		if (preg_match("/\.xml\i", $filename))
		{
			return new XmlParamHandle($filename);
		}

		return new TextParamHandle($filename);
	}

	abstract function write();

	abstract function read();

}

class XmlParamHandle extends ParamHandle
{
	public function write()
	{

	}

	public function read()
	{

	}
}

class TextParamHandle extends ParamHandle
{
	public function write()
	{

	}

	public function read()
	{

	}
}