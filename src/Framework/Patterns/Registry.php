<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 24.01.2018
 * Time: 13:20
 */
namespace Framework\Patterns;

class Registry
{
	/**
	 * @var mixed[]
	 */
	protected static $data = array();

	/**
	 * Добавляет значение в реестр
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return void
	 */
	public static function set($key, $value)
	{
		self::$data[$key] = $value;
	}

	/**
	 * Возвращает значение из реестра по ключу
	 *
	 * @param string $key
	 * @return mixed
	 */
	public static function get($key)
	{
		return isset(self::$data[$key]) ? self::$data[$key] : null;
	}

	/**
	 * Возвращает значения из реестра
	 *
	 * @return array
	 */
	public static function gets()
	{
		return count(self::$data) ? self::$data : null;
	}

	/**
	 * Удаляет значение из реестра по ключу
	 *
	 * @param string $key
	 * @return void
	 */
	final public static function removeProduct($key)
	{
		if (array_key_exists($key, self::$data)) {
			unset(self::$data[$key]);
		}
	}
}