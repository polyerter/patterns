<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 15.11.2018
 * Time: 15:17
 */

namespace Framework\State;


class Close implements State
{
	public function openClose()
	{
		echo "\nДверь закрыта\n";
	}
}