<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 15.11.2018
 * Time: 15:15
 */

namespace Framework\State;


class Door
{
	private $state;

	public function setState(State $state)
	{
		$this->state = $state;
	}

	public function changesState()
	{
		if ($this->state instanceof Open)
		{
			$this->setState(new Close());
		}
		else if ($this->state instanceof Close)
		{
			$this->setState(new Open());
		}
	}

	public function move()
	{
		$this->state->openClose();
	}


}