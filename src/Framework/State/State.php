<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 15.11.2018
 * Time: 15:16
 */

namespace Framework\State;


interface State
{
	public function openClose();
}