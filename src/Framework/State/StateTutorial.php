<?php

namespace Framework\State;


class StateTutorial
{
	public static function main()
	{
		$open = new Open();
		$door = new Door();
		$door->setState($open);

		for ($i = 0; $i < 5; $i++)
		{
			$door->move();
			$door->changesState();
		}
	}

}