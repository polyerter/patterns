<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 12:11
 */

namespace Framework\Strategy;


class CloseA implements Strategy
{
	function openClose()
	{
		echo "\nДверь закрыта методом А\n";
	}
}