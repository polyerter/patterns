<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 12:12
 */

namespace Framework\Strategy;


class DoorA
{
	private $strategy;

	public function setStrategy(Strategy $strategy)
	{
		$this->strategy = $strategy;
	}

	public function onChange()
	{
		$this->strategy->openClose();
	}


}