<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 12:10
 */

namespace Framework\Strategy;


class OpenA implements Strategy
{
	public function openClose()
	{
		echo "\nДверь открыта методом А\n";
	}
}