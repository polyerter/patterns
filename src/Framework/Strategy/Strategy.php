<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 12:10
 */

namespace Framework\Strategy;


interface Strategy
{
	function openClose();
}