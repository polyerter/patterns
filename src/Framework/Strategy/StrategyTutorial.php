<?php
/**
 * Created by PhpStorm.
 * User: polyerter
 * Date: 19.11.2018
 * Time: 12:09
 */

namespace Framework\Strategy;


class StrategyTutorial
{
	public static function main()
	{
		$door = new DoorA();

		$open = new OpenA();
		$door->setStrategy($open);
		$door->onChange();;

		$close = new CloseA();
		$door->setStrategy($close);
		$door->onChange();
	}

}